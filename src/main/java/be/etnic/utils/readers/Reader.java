package be.etnic.utils.readers;

import javax.swing.text.html.Option;
import java.awt.event.KeyEvent;
import java.util.Optional;

public interface Reader {
    Optional<String> readLine();
    Optional<Integer> readInt();
    Optional<Double> readDouble();

}
