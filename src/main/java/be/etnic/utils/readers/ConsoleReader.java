package be.etnic.utils.readers;

import lombok.extern.slf4j.Slf4j;

import java.awt.event.KeyEvent;
import java.io.InputStream;
import java.util.Optional;
import java.util.Scanner;

@Slf4j
public class ConsoleReader implements Reader {
    private final InputStream stream;
    public ConsoleReader() {
        this.stream = System.in;
    }

    private Scanner getScanner() {
        return new Scanner(stream);
    }

    @Override
    public Optional<String> readLine() {
        String read = getScanner().nextLine();
        if (read.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(read);
    }

    @Override
    public Optional<Integer> readInt() {
        String read = getScanner().nextLine();
        if (read.matches("\\d+")) {
            return Optional.of(Integer.parseInt(read));
        }
        log.error("Input is not number");
        return Optional.empty();
    }

    @Override
    public Optional<Double> readDouble() {

        String read = getScanner().nextLine();
        if (read.matches("(\\d+)|(\\d+[\\.\\,]\\d+)")) {
            return Optional.of(Double.parseDouble(read));
        }
        log.error("Input is not number");
        return Optional.empty();
    }
}
