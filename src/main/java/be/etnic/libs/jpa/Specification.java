package be.etnic.libs.jpa;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

@FunctionalInterface
public interface Specification<T> {
    Predicate create(CriteriaBuilder cb, CriteriaQuery<T> cq, Root<T> root);
}
