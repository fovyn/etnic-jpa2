package be.etnic.libs.stores;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public interface IStore<T> {
    static Logger logger = LoggerFactory.getLogger("IStore");

    <U> U dispatch(Action<U> action);

    static <T> IStore<T> createStore(IReducer<T> reducer) {
        logger.info("Store creation");
        return new Store<>(reducer);
    }
}
