package be.etnic.libs.stores;

import javax.persistence.EntityManager;

public interface Action<T> {
    String getAction();
    T execute(EntityManager em);
}
