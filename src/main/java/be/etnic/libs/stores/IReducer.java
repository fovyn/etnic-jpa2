package be.etnic.libs.stores;

import javax.persistence.EntityManager;

public interface IReducer<T> {
    default <U> U reduce(EntityManager em, Action<U> action) { return action.execute(em); }
}
