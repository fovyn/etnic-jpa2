package be.etnic.libs.stores;


import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Store<T> implements IStore<T> {
    private IReducer<T> reducer;

    Store(IReducer<T> reducer) {
        this.reducer = reducer;
    }

    @Override
    public <U> U dispatch(Action<U> action) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("etnicjpa");
        EntityManager em = emf.createEntityManager();

        U result = this.reducer.reduce(em, action);

        em.close();
        emf.close();

        return result;
    }
}
