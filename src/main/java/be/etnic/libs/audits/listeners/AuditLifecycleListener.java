package be.etnic.libs.audits.listeners;

import be.etnic.libs.audits.CreatedAt;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;
import java.lang.reflect.Field;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

@Slf4j
public class AuditLifecycleListener {

    @PrePersist
    public void prePersistAction(Object target) {
        log.info("PrePersist => {}", target);
        Stream<Field> parentFields = Arrays.stream(target.getClass().getSuperclass().getDeclaredFields());
        Stream<Field> fields = Arrays.stream(target.getClass().getDeclaredFields());

        Stream
                .concat(parentFields, fields)
                .filter(it -> it.isAnnotationPresent(CreatedAt.class))
                .forEach(field -> {
                    try {
                        field.setAccessible(true);
                        field.set(target, LocalDate.now());
                    } catch (Exception ignored) {

                    }
                });

        log.info("PrePersist => {}", target);
    }
}
