package be.etnic.libs.containers;

import org.reflections.Reflections;

import java.lang.reflect.Constructor;
import java.lang.reflect.Parameter;
import java.util.*;
import java.util.function.Consumer;

public final class DIContainer {
    private Map<Class<?>, Object> services = new HashMap<>();

    public void init() {
        Reflections reflections = new Reflections("be.etnic");
        initActions(reflections);
        initReducers(reflections);
        initStores(reflections);
    }

    private void initActions(Reflections reflections) {
        Set<Class<?>> actions = reflections.getTypesAnnotatedWith(Action.class);

        actions.forEach(this::add);
    }

    private void initReducers(Reflections reflections) {
        Set<Class<?>> reducers = reflections.getTypesAnnotatedWith(Reducer.class);
        reducers.forEach(this::add);
    }

    private void initStores(Reflections reflections) {
        Set<Class<?>> stores = reflections.getTypesAnnotatedWith(Store.class);
        stores.forEach(this::add);
    }

    public void add(Class<?> service) {
        this.services.put(service, resolve(service));
    }

    private void handleParameter(List<Object> pValues, Parameter parameter, Consumer<Class<?>> consumer) {
        Class<?> pType = parameter.getType();
        if (!this.has(pType)) {
            consumer.accept(pType);
        }
        Object pValue = this.get(pType);
        pValues.add(pValue);
    }

    public <T> T get(Class<?> service) {
        return (T) this.services.get(service);
    }

    public boolean has(Class<?> service) {
        return this.services.containsKey(service);
    }

    public <T> T resolve(Class<T> tClass) {
        try {
            Constructor<T> constructor = (Constructor<T>) tClass.getConstructors()[0];
            Parameter[] parameters = constructor.getParameters();
            T instance = null;
            if (parameters.length == 0) {
                instance = constructor.newInstance();
                return instance;
            }

            List<Object> pValues = new ArrayList<>();
            for(Parameter parameter: parameters) {
                handleParameter(pValues, parameter, (pType) -> pValues.add(this.resolve(pType)));
            }

            return constructor.newInstance(pValues.toArray());
        } catch (Exception ignored) {
            throw new RuntimeException("Cannot be resolve type: "+ tClass.getName());
        }
    }
}
