package be.etnic.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import java.io.Serializable;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Pilot extends Person implements Serializable {
    private String certificate;

    public static Predicate nameLike(CriteriaBuilder cb, Root<Pilot> root) {
        return cb.like(root.get(Pilot_.name), "Fla%");
    }
}
