package be.etnic.entities;

import javax.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@Entity
@Table(
        name = "etnic_plane",
        indexes = {},
        uniqueConstraints = {
                @UniqueConstraint(name = "UK_Plane_imma", columnNames = {"plane_imma"})
        }
)
public class Plane implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "plane_imma")
    private String imma;


    @ManyToOne(targetEntity = Person.class, fetch = FetchType.LAZY)
    private Person owner;

    @ManyToOne(targetEntity = PlaneType.class, fetch = FetchType.LAZY)
    private PlaneType type;

    @Override
    public String toString() {
        return new StringBuilder("Plane<id: ").append(id)
                .append(", imma: '").append(imma).append("'")
                .append(", type: ").append(type)
                .append(">").toString();
    }
}
