package be.etnic.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import java.io.Serializable;

@Entity
@Data
public class Flight implements Serializable {
    @Id
    private Long id;

    @ManyToOne(targetEntity = Pilot.class)
    private Pilot pilot;
    @ManyToOne(targetEntity = PlaneType.class)
    private PlaneType planeType;

    private int flightDuration;
}
