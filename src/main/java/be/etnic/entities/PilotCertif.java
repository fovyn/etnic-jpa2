package be.etnic.entities;

import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;

@Entity
@IdClass(PilotCertif.PilotCertifId.class)
public class PilotCertif implements Serializable {
    @Id
    @Column(name = "pilot_id", insertable = false, updatable = false)
    private Long pilotId;
    @Id
    @Column(name = "planeType_id", insertable = false, updatable = false)
    private Long planeTypeId;
    private LocalDate gradeDate;

    @ManyToOne
    @JoinColumn(name = "pilot_id")
    private Pilot pilot;
    @ManyToOne
    @JoinColumn(name = "planeType_id")
    private PlaneType planeType;

    @EqualsAndHashCode
    @ToString
    public static class PilotCertifId implements Serializable {
        private Long pilotId;
        private Long planeTypeId;
    }
}
