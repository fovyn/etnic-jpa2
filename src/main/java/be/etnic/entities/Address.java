package be.etnic.entities;

import javax.persistence.*;

import lombok.Data;

@Embeddable
@Data
public class Address {
    private String street;
//    @Column(name = "address_number")
    private String number;
//    @Column(name = "address_city")
    private String city;
//    @Column(name = "address_country")
    private String country;
}
