package be.etnic.entities;

import be.etnic.libs.audits.CreatedAt;
import be.etnic.libs.audits.listeners.AuditLifecycleListener;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
@Data
@EqualsAndHashCode(of = {"name"})
@EntityListeners(value = {AuditLifecycleListener.class})
public class Person implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String phoneNumber;

    @Embedded
    private Address address;

    @CreatedAt
    private LocalDate createdAt;

//    @OneToMany(targetEntity = Plane.class, mappedBy= "owner")
//    private List<Plane> planes = new ArrayList<>();

    @PrePersist
    public void prePersistAction() {
        System.out.println("Blop");
    }
}
