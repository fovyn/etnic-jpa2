package be.etnic.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
@Data
@EqualsAndHashCode
public class Sustaining implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Lob
    private String object;

    @ManyToOne(targetEntity = Maintener.class)
    private Maintener worker;
    private LocalDateTime workDate;
    private LocalTime workTime;

    @ManyToOne(targetEntity = Maintener.class)
    private Maintener checker;
    private LocalDateTime checkDate;

    @PrePersist
    public void prePersistAction() {
        this.workDate = LocalDateTime.now();
    }


}
