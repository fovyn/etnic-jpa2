package be.etnic.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;

import java.io.Serializable;

@Entity
@Data
@EqualsAndHashCode(callSuper = true)
public class Maintener extends Person implements Serializable {

}
