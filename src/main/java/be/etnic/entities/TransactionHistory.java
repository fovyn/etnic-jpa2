package be.etnic.entities;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Data
@NamedStoredProcedureQueries(value = {
        @NamedStoredProcedureQuery(
                name = "newTransaction",
                procedureName = "sp_newtransaction",
                parameters = {
                        @StoredProcedureParameter(name = "plane", type = Integer.class, mode = ParameterMode.IN),
                        @StoredProcedureParameter(
                                name = "newOwnerId",
                                type = Integer.class,
                                mode = ParameterMode.IN
                        )
                }
        )
})
public class TransactionHistory implements Serializable {
    @Id
    private Long planeId;
    @Id
    private Long ownerId;

    private LocalDate transactionDate;
}
