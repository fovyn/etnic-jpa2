package be.etnic;

import be.etnic.entities.Pilot;
import be.etnic.entities.Pilot_;
import be.etnic.entities.Plane;
import be.etnic.libs.stores.IStore;
import be.etnic.stores.plane.PlaneActions;
import be.etnic.stores.plane.PlaneReducer;
import lombok.extern.slf4j.Slf4j;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

import static be.etnic.libs.stores.IStore.createStore;

@Slf4j
public class Main {
    public static void main(String[] args) {
        IStore<Plane> planeStore = createStore(new PlaneReducer());



        List<Plane> planes = planeStore.dispatch(PlaneActions.readAll());

//        log.info("{}", planes.getImma());
    }
}