package be.etnic.stores.plane;

import be.etnic.entities.Plane;
import be.etnic.libs.stores.Action;

import javax.persistence.EntityManager;
import javax.persistence.StoredProcedureQuery;
import java.util.List;

class ReadAllAction implements Action<List<Plane>> {
    @Override
    public String getAction() {
        return "[Plane] - ReadAll";
    }

    @Override
    public List<Plane> execute(EntityManager em) {
        return em.createQuery("SELECT p FROM Plane p", Plane.class).getResultList();
    }
}
