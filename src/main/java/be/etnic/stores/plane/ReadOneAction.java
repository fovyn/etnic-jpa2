package be.etnic.stores.plane;

import be.etnic.entities.Plane;
import be.etnic.libs.jpa.Specification;
import be.etnic.libs.stores.Action;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class ReadOneAction implements Action<Plane> {
    private List<Specification<Plane>> predicates = new ArrayList<>();

    public ReadOneAction(Specification<Plane>... predicates) {
        this.predicates.addAll(Arrays.asList(predicates));
    }

    @Override
    public String getAction() {
        return "[Plane] - ReadOne";
    }

    @Override
    public Plane execute(EntityManager em) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<Plane> criteriaQuery = builder.createQuery(Plane.class);
        Root<Plane> root = criteriaQuery.from(Plane.class);

        Stream<Predicate> predicates = this.predicates.stream().map(it -> it.create(builder, criteriaQuery, root));
        return em.createQuery(
                criteriaQuery.where(predicates.toArray(Predicate[]::new))
        ).getSingleResult();
    }
}
