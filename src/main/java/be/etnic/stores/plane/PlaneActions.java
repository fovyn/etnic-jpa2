package be.etnic.stores.plane;

import be.etnic.entities.Plane;
import be.etnic.entities.PlaneType;
import be.etnic.entities.PlaneType_;
import be.etnic.entities.Plane_;
import be.etnic.libs.stores.Action;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import java.util.List;

public abstract class PlaneActions {

    public static Action<List<Plane>> readAll() {
        return new ReadAllAction();
    }
    public static Action<Plane> readOne(String imma) {
        return new ReadOneAction(
                (cb, cq, root) -> cb.equal(root.get(Plane_.imma), imma)
        );
    }
    public static Action<Plane> readOneByImmaAndType(String imma, String type) {
        return new ReadOneAction(
                (cb, cq, root) -> cb.equal(root.join(Plane_.type).get(PlaneType_.name), type),
                (cb, cq, root) -> cb.like(root.get(Plane_.imma), "%"+ imma+ "%")
        );
    }
}
