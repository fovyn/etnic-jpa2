package be.etnic.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Plane.class)
public abstract class Plane_ {

	public static volatile SingularAttribute<Plane, Person> owner;
	public static volatile SingularAttribute<Plane, String> imma;
	public static volatile SingularAttribute<Plane, Integer> id;
	public static volatile SingularAttribute<Plane, PlaneType> type;

	public static final String OWNER = "owner";
	public static final String IMMA = "imma";
	public static final String ID = "id";
	public static final String TYPE = "type";

}

