package be.etnic.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PlaneType.class)
public abstract class PlaneType_ {

	public static volatile SingularAttribute<PlaneType, String> brend;
	public static volatile SingularAttribute<PlaneType, Short> nbPlace;
	public static volatile SingularAttribute<PlaneType, String> name;
	public static volatile SingularAttribute<PlaneType, Integer> id;
	public static volatile SingularAttribute<PlaneType, Integer> enginePower;

	public static final String BREND = "brend";
	public static final String NB_PLACE = "nbPlace";
	public static final String NAME = "name";
	public static final String ID = "id";
	public static final String ENGINE_POWER = "enginePower";

}

