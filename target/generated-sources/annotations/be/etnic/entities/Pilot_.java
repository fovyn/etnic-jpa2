package be.etnic.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Pilot.class)
public abstract class Pilot_ extends be.etnic.entities.Person_ {

	public static volatile SingularAttribute<Pilot, String> certificate;

	public static final String CERTIFICATE = "certificate";

}

