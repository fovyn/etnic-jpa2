package be.etnic.entities;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(PilotCertif.class)
public abstract class PilotCertif_ {

	public static volatile SingularAttribute<PilotCertif, Long> planeTypeId;
	public static volatile SingularAttribute<PilotCertif, Pilot> pilot;
	public static volatile SingularAttribute<PilotCertif, LocalDate> gradeDate;
	public static volatile SingularAttribute<PilotCertif, PlaneType> planeType;
	public static volatile SingularAttribute<PilotCertif, Long> pilotId;

	public static final String PLANE_TYPE_ID = "planeTypeId";
	public static final String PILOT = "pilot";
	public static final String GRADE_DATE = "gradeDate";
	public static final String PLANE_TYPE = "planeType";
	public static final String PILOT_ID = "pilotId";

}

