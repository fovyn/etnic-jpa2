package be.etnic.entities;

import java.time.LocalDate;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(TransactionHistory.class)
public abstract class TransactionHistory_ {

	public static volatile SingularAttribute<TransactionHistory, Long> planeId;
	public static volatile SingularAttribute<TransactionHistory, Long> ownerId;
	public static volatile SingularAttribute<TransactionHistory, LocalDate> transactionDate;

	public static final String PLANE_ID = "planeId";
	public static final String OWNER_ID = "ownerId";
	public static final String TRANSACTION_DATE = "transactionDate";

}

