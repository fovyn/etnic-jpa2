package be.etnic.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Flight.class)
public abstract class Flight_ {

	public static volatile SingularAttribute<Flight, Pilot> pilot;
	public static volatile SingularAttribute<Flight, Integer> flightDuration;
	public static volatile SingularAttribute<Flight, Long> id;
	public static volatile SingularAttribute<Flight, PlaneType> planeType;

	public static final String PILOT = "pilot";
	public static final String FLIGHT_DURATION = "flightDuration";
	public static final String ID = "id";
	public static final String PLANE_TYPE = "planeType";

}

