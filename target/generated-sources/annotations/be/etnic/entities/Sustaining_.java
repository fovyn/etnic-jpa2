package be.etnic.entities;

import java.time.LocalDateTime;
import java.time.LocalTime;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Sustaining.class)
public abstract class Sustaining_ {

	public static volatile SingularAttribute<Sustaining, LocalDateTime> workDate;
	public static volatile SingularAttribute<Sustaining, Long> id;
	public static volatile SingularAttribute<Sustaining, Maintener> checker;
	public static volatile SingularAttribute<Sustaining, Maintener> worker;
	public static volatile SingularAttribute<Sustaining, LocalDateTime> checkDate;
	public static volatile SingularAttribute<Sustaining, LocalTime> workTime;
	public static volatile SingularAttribute<Sustaining, String> object;

	public static final String WORK_DATE = "workDate";
	public static final String ID = "id";
	public static final String CHECKER = "checker";
	public static final String WORKER = "worker";
	public static final String CHECK_DATE = "checkDate";
	public static final String WORK_TIME = "workTime";
	public static final String OBJECT = "object";

}

